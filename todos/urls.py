from django.urls import path
from todos.views import (
    todo_lists,
    get_list,
    create_list,
    update_list,
    delete_list,
    create_item,
    update_item,
)

urlpatterns = [
    path("", todo_lists, name="todolist"),
    path("<int:id_value>/", get_list, name="getlist"),
    path("create/", create_list, name="createlist"),
    path("<int:id_value>/edit", update_list, name="updatelist"),
    path("<int:id_value>/delete/", delete_list, name="deletelist"),
    path("createitem/", create_item, name="createitem"),
    path("items/<int:id_value>/edit/", update_item, name="todo_item_update"),
]
