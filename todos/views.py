from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import NewListForm, NewItemForm


# Create your views here.
def todo_lists(request):

    list = TodoList.objects.all
    context = {"list": list}
    return render(request, "todos/list.html", context)


def get_list(request, id_value):

    lists = get_object_or_404(TodoList, id=id_value)

    context = {"one_list": lists}

    return render(request, "todos/details.html", context)


def create_list(request):
    if request.method == "POST":
        form = NewListForm(request.POST)
        if form.is_valid():
            list = form.save(False)
            list.user = request.user
            list.save()
            return redirect("getlist", list.id)

    else:
        form = NewListForm()

    context = {"create": form}

    return render(request, "todos/create.html", context)


def update_list(request, id_value):
    if request.method == "POST":
        todo = TodoList.objects.get(id=id_value)
        form = NewListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("getlist", todo.id)
    else:
        form = NewListForm()

    context = {"edit": form}
    return render(request, "todos/edit.html", context)


def delete_list(request, id_value):
    list = TodoList.objects.get(id=id_value)
    if request.method == "POST":
        list.delete()
        return redirect("todolist")

    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = NewItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("getlist", item.list.id)

    else:
        form = NewItemForm()

    context = {"createitem": form}

    return render(request, "todos/create_item.html", context)


def update_item(request, id_value):
    todo = TodoItem.objects.get(id=id_value)
    if request.method == "POST":
        form = NewItemForm(request.POST, instance=todo)
        if form.is_valid():
            item = form.save()
            return redirect("getlist", item.list.id)
    else:
        form = NewItemForm()

    context = {
        "updateitem": form,
    }
    return render(request, "todos/update_item.html", context)
