from todos.models import TodoList, TodoItem
from django.forms import ModelForm


class NewListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ("name",)


class NewItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ("task", "due_date", "is_completed", "list")
